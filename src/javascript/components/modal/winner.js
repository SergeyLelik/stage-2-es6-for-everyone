import {createElement} from '../../helpers/domHelper';
import {showModal} from './modal';

export function showWinnerModal(fighter) {
  // call showModal function 

  const title = `${fighter.name} Winner!!!!!`;
  const blockWinner = createElement({ tagName: 'H1', className: 'modal-body' });
  blockWinner.innerText = "Winner!!!! Name: " + fighter.name;
  const result = {
    title,
    bodyElement: blockWinner,
    onClose: () => {
      window.location.reload();
    },
  };
  showModal(result);
}
